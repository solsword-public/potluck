"""
Potluck configuration (for testarea examples).

potluck_config.py
"""

# We override the submissions directory setting with our examples
# directory, since everything in this test area is an example, so it
# doesn't make sense to have separate examples and submissions.
SUBMISSIONS_DIR = "examples"
