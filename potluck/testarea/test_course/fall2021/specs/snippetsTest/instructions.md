## Snippets Test Task

Instructions for the snippets test task.

This is [markdown](https://python-markdown.github.io/reference/).

We can link to a rubric item and/or a snippet:

- [You have to call sorted in the definition of processData](#goal:core.check:def-processData:call-sorted)
- [Your `processData` function has to return the correct results.](#goal:core.test:processData)
- [Your file `process.py` must work correctly when run with inputs.](#goal:extra.test:import)
- [Examples of the outputs for the `processData` function.](#snippet:examples)

TODO: Highlight rubric items & examples when linking to them; add shiny
to rubric links via JS!

Now

\ 

I'll

\ 

make

\ 

these

\ 

longer

\ 

so

\ 

the

\ 

rubric

\ 

get

\ 

pushed

\ 

down...
