For this task, you will need to write four functions:

1. `indentMessage` - A function which
    [adds spaces to indent a string](#snippet:indentMessage) to a
    certain minimum length.
2. `printMessage` - Uses `indentMessage` to
    [indent a message and also prints it](#snippet:printMessage).
3. `ellipseArea` - Calculates and returns
    [the area of an ellipse](#snippet:ellipseArea) given its radii.
4. `polygon` - Uses `turtle` functions to
    [draw a polygon](#snippet:polygon).

Note: these instructions are not really detailed enough for a real
assignment!
