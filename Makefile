README.html: README.md readable.css
	pandoc $< --standalone --from=markdown+smart --to=html --css readable.css --output=$@
	chmod 0755 $@


# Note: We just shell-game the test area out of the way since pdoc tries
# to import the files in it causing all sorts of issues...
docs: potluck/*.py potluck_server/*.py
	mv potluck/testarea ./._testarea
	PYTHONPATH=. pdoc --output-dir docs ./potluck ./potluck_server \
	  || mv ./._testarea potluck/testarea
	mv ./._testarea potluck/testarea
	chmod -R 0755 docs
