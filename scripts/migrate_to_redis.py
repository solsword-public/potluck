#!/usr/bin/env python
"""
Script for migrating from file-based sync.py server storage backend to
Redis-and-file-based storage.py backend. Loads file-based info that
should be stored in Redis and saves it into Redis. Uses ps_config.py to
figure out the Redis port, so run this in the new server's rundir.

Seprately, logs, reports, and submisisons should be rsynced to the new
evaluation directory.

FIRST, edit the config variables below to match where you're migrating from.
"""

import os, json, glob

import flask # to read config file

from potluck_server import storage

# Config

old_eval_dir = "/home/potluck/evaluation"
course = "cs111"
semester = "fall21"

# Read server config
app = flask.Flask('config_import')
app.config.from_object('ps_config')

# Set up Redis connection
storage.init(app.config)

where = os.path.join(old_eval_dir, course, semester)


# Inflight files
ifdir = os.path.join(where, 'inflight')
for iff in os.listdir(ifdir):
    username = iff.split('-')[1]
    with open(os.path.join(ifdir, iff), 'r') as fin:
        if_info = json.load(fin)

    for phase in if_info:
        psets = if_info[phase]
        for psid in psets:
            tasks = psets[psid]
            for taskid in tasks:
                iflist = tasks[taskid]
                # Edit directly
                key = storage.inflight_key(
                    course,
                    semester,
                    username,
                    phase,
                    psid,
                    taskid
                )
                storage._REDIS.delete(key)
                storage._REDIS.rpush(key, *iflist)


# Extension files
extdir = os.path.join(where, 'extensions')
for extf in os.listdir(extdir):
    username = os.path.splitext(extf)[0]
    with open(os.path.join(extdir, extf), 'r') as fin:
        ext_info = json.load(fin)

    for phase in ext_info:
        psets = ext_info[phase]
        for psid in psets:
            storage.set_extension(
                course,
                semester,
                username,
                psid,
                phase,
                duration=psets[psid]
            )

# Task info files
subdir = os.path.join(where, 'submissions')
for username in os.listdir(subdir):
    for tif in glob.glob(os.path.join(subdir, username, '*-info.json')):
        with open(tif, 'r') as fin:
            tinfo = json.load(fin)

        storage.record_time_spent(
            course,
            semester,
            username,
            tinfo['phase'],
            tinfo['psid'],
            tinfo['taskid'],
            tinfo['time_spent']
        )
